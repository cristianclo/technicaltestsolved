import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import json
import uuid
import re
from multiprocessing import Pool

API_KEY = "YOUR_API_KEY_HERE"

### 1. Consuming a GET API service (http://34.201.19.114:40003/recordController/getAllRecords) to extract coordinates.


def slice(lista, num_elements_slice, output):
    """ Function that receives a list -list-, a number of partitions in the list and an empty list -output- where the partitions are added.
    """
    lista = list(lista)
    if lista != []:
        values = lista[0:num_elements_slice]
        output.append(values)
        lista = lista[num_elements_slice:]
        return slice(lista, num_elements_slice, output)
    return output

def filterRecord(record):
	""" Returns a dictionary: {'longitude': float, 'latitude': float} if the record contains a valid ('longitude', float) and ('latitude', float) else returns an empty dictionary
    """
	valid_record = dict()
	record_keys = record.keys()

	if (('latitude' in record_keys) and ('longitude' in record_keys)):
		if (isinstance(record['latitude'], float) and isinstance(record['longitude'], float)):
			valid_record['latitude'] = record['latitude']
			valid_record['longitude'] = record['longitude']
	return valid_record

def filterValidRecords(records):
	""" Function that receives all the records and return the valid ones [{'latitude': float, 'longitude': float}]
    """
	valid_records = []

	for record in records:
		filteredRecord = filterRecord(record)
		if(len(filteredRecord)):
			valid_records.append(filteredRecord)
	return valid_records

def buildLocationParam(records):
	""" Function that receives a list with the valid records and returns a -string- 'latitude,longitude|latitude,longitude|latitude,longitude|...' necessary for the Maps API request
    """
	return "|".join( ",".join([str(record['latitude']), str(record['longitude'])]) for record in records)

def getLocations(records):
	""" Function that receives all the records and returns a dictionary with the elevation, location and resolution coordinates from all the valid records (latitude, longitude)
    """
	### Maps Elevation API endpoint
	url_gm = "https://maps.googleapis.com/maps/api/elevation/json"

	### Since the records dictionary contains keys that we don't need to process the request to the API,
	### we need to extract just the latitude and longitude from every record that applies.
	pool = Pool(processes=16)
	valid_records = []

	sliced_records = slice(records, 200, [])### Slicing records in order to process them in parallel
	validation = pool.map(filterValidRecords, sliced_records)### Parallel processing records to filter valid ones

	for parallel_validation in validation:
		valid_records.append(parallel_validation)

	locationParams = ""
	buildedLocationParams = pool.map(buildLocationParam, valid_records)### Parallel processing valid records to concatenate them

	for buildedLocationParam in buildedLocationParams:
		locationParams += buildedLocationParam

	if(len(locationParams)):

		### Paylaod - Dict to be sent to the Maps API
		PARAMS = {"locations": locationParams, "key": API_KEY}

		### Request to Google Maps API
		maps_request = requests.get(url=url_gm, params=PARAMS)

		if(maps_request.status_code == 200):
			### API Response
			response = maps_request.json()
			
			print(response)
			return response
		else:
			print("La API de Google dejó de responder.")
			return
	else:
		print("Sin registros para consultar.")
		return

def getCoordinates():
	""" Function that gets all the records, send them to the Maps API request and returns the respective response
    """
	### Records endpoint
	url_r = 'https://34.201.19.114:40003/recordController/getAllRecords'

	### Records
	records_request = requests.get(url=url_r, verify=False) #  Adding verify = False in order to not to validate SSL certificate, not recommendable but necessary to communicate with the service.

	if(records_request.status_code == 200):### API status response validation in order to guarantee that it was successful.
		records = records_request.json()
		if(records):
			getLocations(records)
		else:
			print("No hay registros a procesar.")
			return
	else:
		print("Error ", records_request.status_code,". La API no respondió correctamente.")
		return


getCoordinates()



##### 2. Create a POST service

def createRecord(new_record):
	""" Receives a JSON with that complies with the required_fields and returns the id of the new record inserted, else returns the specified error.
	"""
	### {"id":"84ecadcc-1422-45f5-38e9-e204d84636b0","uuid":None,"categoryA":"A","categoryB":"A2","categoryC":None,"subCategory":None,"institutionName":None,"comments":"Los buses estám muy viejos","politicA":None,"politicB":None,"politicC":None,"anonymus":false,"name":"Cristian Andres Prueba Prueba","email":"prueba@gmail.com","phone":"460704356485","code_state":"25","code_city":"1","gender":"M","ip_address":"79.60.19.171","ip_info":"Mozilla/5.0 (Linux; Android 8.0.0; WAS-LX3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.143 Mobile Safari/537.36","date":1563479108000,"image":None,"staName":"NORTE DE SANTANDER","citName":"CUCUTA","citId":None,"latitude":7.889097,"longitude":-72.4966896,"locInfo":None,"countItems":0,"fileRecords":None}
	### New record fields
	new_record_fields = new_record.keys()

	### Required & allowed fields to make the POST request - It doesn't receive the id nor uuid because it'll be autogenerated
	required_fields = ["categoryA", "categoryB", "categoryC", "subCategory", "institutionName", "comments", "politicA", "politicB", "politicC", "anonymus", "name", "email", "phone", "code_state", "code_city", "gender", "ip_address", "ip_info", "date", "image", "staName", "citName", "citId", "latitude", "longitude", "locInfo", "countItems", "fileRecords"]

	### Checking if the new record contains all the required & allowed fields
	matched_fields = set(new_record_fields).union(set(required_fields))
	if(len(matched_fields) == len(required_fields)):
		print(len(matched_fields), len(required_fields))
		### UUID
		uid = str(uuid.uuid4())

		### Adding ID to new record
		new_record["id"] = uid
		new_record["uuid"] = uid

		### API-endpoint to create new records
		url_c = 'https://34.201.19.114:40003/recordController/createRecord'

		print("new record", new_record)

		### Creating new record
		new_record_request = requests.post(url=url_c, json=new_record, verify=False)

		if(new_record_request.status_code == 200):

			new_record_response = new_record_request.text
			if(new_record_response == 'BELLO'):### I can't assure that this answer means that the post request was successful and the record was created, but I assume this answer as correct in order to continue...
				response = {'id': uid}
				print(response)
				return uid
		print("Ocurrió un error consultando la API")
		return
	else:
		print("No es posible crear el nuevo registro debido a que los campos otorgados no coinciden con los requeridos/permitidos.")
		return

	
new = {"categoryA":"A","categoryB":"A2","categoryC":None,"subCategory":None,"institutionName":None,"comments":"Los buses estám muy viejos","politicA":None,"politicB":None,"politicC":None,"anonymus":False,"name":"Cristian Andres Prueba Prueba","email":"prueba@gmail.com","phone":"460704356485","code_state":"25","code_city":"1","gender":"M","ip_address":"79.60.19.171","ip_info":"Mozilla/5.0 (Linux; Android 8.0.0; WAS-LX3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.143 Mobile Safari/537.36","date":1563479108000,"image":None,"staName":"NORTE DE SANTANDER","citName":"CUCUTA","citId":None,"latitude":7.889097,"longitude":-72.4966896,"locInfo":None,"countItems":0,"fileRecords":None}
#createRecord(new)

### 3. regex


def googleSpelling(line):

	regex = "^[gG](o|O|0|\(\)|\[\]|\<\>){2}[gG][lLI][eE3]$"
	if re.match(regex, line):
		return True
	return False

print(googleSpelling("g()()GI3"))