# https://directorio-empresas.einforma.co/informacion-empresa/gonzalez-velasquez-bolivar-ltda


# Razón social
# Teléfono
# Ciudad
# Departamento
# Dirección actual
# NIT
# Actividad
# Forma jurídica
# Ranking de Empresas

# proxyswitching y cambiar de UserAgent

import os
import re
import requests
import time
import pandas as pd
import concurrent.futures
from selenium import webdriver
from bs4 import BeautifulSoup


def processCompany(url):
	
	request = requests.get(url)
	company = request.text

	soup = BeautifulSoup(company, "lxml")

	tables = soup.find_all('table', class_='informe')

	telephone_element = soup.find(id='myTelephone')
	city_element = soup.find(id='situation_loc')
	department_element = soup.find(id='situation_prov')
	address_element = soup.find(id='situation_calle')

	telephone = telephone_element.contents[0]
	city = city_element.contents[0]
	department = department_element.contents[0]
	address = address_element.contents[0]

	relevant_info = {'telefono': telephone, 'ciudad': city, 'departmento': department, 'direccion': address}

	for i in range(2):
		table = tables[i]
		tr_elements = table.find_all('tr')
		data = []
		data_to_check = {'Razón Social': 'razon_social', 'NIT': 'NIT', 'Actividad': 'actividad', 'Forma jurídica': 'forma_juridica', 'Ranking de Empresas': 'ranking_empresas'}
		for row in tr_elements[1:]:
		    cols = row.find_all('td')
		    cols = [ele.text.strip() for ele in cols]
		    if cols[0] in data_to_check.keys():
		    	key = data_to_check[cols[0]]
		    	value = cols[1]
		    	relevant_info[key] = value
		    data.append([ele for ele in cols if ele])

		#print(data)
	print(relevant_info)
	return relevant_info

#url = "https://directorio-empresas.einforma.co/informacion-empresa/gonzalez-velasquez-bolivar-ltda"
#processCompany(url)

def proccessPage(url, timeout):

	HEADERS = {'user-agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5)'
                          'AppleWebKit/537.36 (KHTML, like Gecko)'
                          'Chrome/45.0.2454.101 Safari/537.36'),
                          'referer': 'http://stats.nba.com/scores/'}

	print(url)
	url_to_access = url
	request = requests.get(url_to_access, headers=HEADERS, timeout=timeout)
	page_content = request.text

	soup = BeautifulSoup(page_content, "lxml")
	links_by_company = [[element.string, element['href']] for element in soup.find_all('a', id=re.compile('^link-emp-'))]
	
	print(links_by_company)
	return links_by_company


def processHome():

	url = 'https://directorio-empresas.einforma.co/'
	chrome_driver_path = os.path.abspath('chromedriver')

	driver = webdriver.Chrome(chrome_driver_path)
	driver.implicitly_wait(30)
	driver.get(url)

	# Find next element to click (https://directorio-empresas.einforma.co/actividad/F-construccion/ - href="/actividad/F-construccion/")
	href = '/actividad/F-construccion/'

	element = driver.find_element_by_css_selector("a[href*='"+href+"']")
	element.click()

	# Getting links from every page (https://directorio-empresas.einforma.co/actividad/F-construccion/?qPg=#)

	page_base_url = 'https://directorio-empresas.einforma.co/actividad/F-construccion/?qPg='
	urls_to_access = [page_base_url + str(index + 1) for index in range(200)]


	print("### START - Getting links from every page ###")

	CONNECTIONS = 200
	TIMEOUT = 5
	out = []

	with concurrent.futures.ThreadPoolExecutor(max_workers=CONNECTIONS) as executor:
	    future_to_url = (executor.submit(proccessPage, url, TIMEOUT) for url in urls_to_access)
	    time1 = time.time()
	    for future in concurrent.futures.as_completed(future_to_url):
	        try:
	            data = future.result()
	        except Exception as exc:
	            data = str(type(exc))
	        finally:
	            out.append(data)

	            print(str(len(out)),end="\r")

	    time2 = time.time()

	print(f'Took {time2-time1:.2f} s')

	print("### END - Getting links from every page ###")




	# home = driver.page_source
	# soup = BeautifulSoup(home, "lxml")

	# tables = soup.find_all('table', class_='informe')

	# # Table that contains companies information by their activity
	# companies_by_activity = tables[0]

	# F_construccion = companies_by_activity.find_all(text=re.compile('Construccion'))
	# td = [score.parent for score in F_construccion]


	# print(td)


	# request = requests.get(url)
	# home = request.text

	# soup = BeautifulSoup(home, "lxml")

	# tables = soup.find_all('table', class_='informe')

	# print(tables)


processHome()